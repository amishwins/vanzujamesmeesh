//
//  BusStop.h
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BusStop : NSObject

-(id)initWithID:(NSString *)busStopID;
-(NSArray *)retrieveNextTimesForCurrentTimeAndDate;

@end
