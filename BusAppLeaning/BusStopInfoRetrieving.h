//
//  BusStopInfoRetrieving.h
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

// Do we need this forward declaration?
//@class BusStop;

@protocol BusStopInfoRetrieving <NSObject>

-(BusStop *)getBusStop:(NSString *)busStopCode;

@end
