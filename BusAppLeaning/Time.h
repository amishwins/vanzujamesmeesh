//
//  Time.h
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Time : NSObject

@property int hours, minutes;

-(id)initWithHour:(int)hour andMinute:(int)minute;

@end
