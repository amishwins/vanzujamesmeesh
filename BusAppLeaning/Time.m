//
//  Time.m
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Time.h"

@implementation Time

@synthesize hours = _hours;
@synthesize minutes = _minutes;

-(id)initWithHour:(int)hour andMinute:(int)minute {
    self.hours = hour;
    self.minutes = minute;
    return self;
}

@end
