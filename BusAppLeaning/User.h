//
//  User.h
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject



- (NSArray *)getAllBusStops;
- (void)addBusStop:(NSString *)busStopID;

@end
