//
//  User.m
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "User.h"
#import "BusStop.h"

@interface User()

@property (nonatomic, strong) NSMutableArray *busStops;

@end

@implementation User

@synthesize busStops;


-(id)init {
    self = [super init];
    
    if (self) {
        busStops = [[NSMutableArray alloc]init];        
    }
    
    return self;
}

-(NSArray *)getAllBusStops {
    
    return busStops;
}

-(void)addBusStop:(NSString *)busStopID {
    BusStop *newBusStop = [[BusStop alloc]initWithID:busStopID];
    [busStops addObject:newBusStop];
}

@end
