//
//  BusStopsScheduleTests.m
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BusStopsScheduleTests.h"
#import "BusStop.h"
#import "Schedule.h"
#import "Time.h"

@implementation BusStopsScheduleTests

// All code under test must be linked into the Unit Test bundle
- (void)testBusStopScheduleHasValidHourAndTime
{
    BusStop *myBusStop = [[BusStop alloc]initWithID:@"1"];
    NSDate *today = [NSDate date];
    Time *now = [[Time alloc]initWithHour:10 andMinute:10];
    NSArray *nextThreeTimes = [myBusStop retrieveNextTimesForCurrentTimeAndDate];

    Time *firstTime = [[Time alloc]initWithHour:<#(int)#> andMinute:<#(int)#>;
    Time *secondTime = [[Time alloc]initWithHour:<#(int)#> andMinute:<#(int)#>;
    Time *thirdTime = [[Time alloc]initWithHour:<#(int)#> andMinute:<#(int)#>;
                       
    NSArray *expectedReturnTimes = [[NSArray alloc]initWithObjects:firstTime, secondTime, thirdTime, nil];


}



@end
