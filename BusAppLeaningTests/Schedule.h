//
//  Schedule.h
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Schedule : NSObject

@property (strong) NSMutableArray *times;

@end
