//
//  UserTests.m
//  BusAppLeaning
//
//  Created by Gala, Amish on 1/8/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "UserTests.h"
#import "User.h"
#import "BusStop.h"

@implementation UserTests


-(void)setUp {
    NSLog(@"User Test setUp");
}

-(void)tearDown {
    NSLog(@"User Test tearDown");
}

// All code under test must be linked into the Unit Test bundle
- (void)testMath
{
    STAssertTrue((1 + 1) == 2, @"Compiler isn't feeling well today :-(");
}


- (void)testGetBusStopsShouldReturnEmpty {
    User *myUser = [[User alloc]init];
    NSArray *busStops = [myUser getAllBusStops];
    STAssertEquals(0, (int)[busStops count], @"Bus stops contained stops incorrectly");
}

- (void)testAddBusStopToFollowVerifyItHasBeenAdded {
    User *myUser = [[User alloc]init];
    [myUser addBusStop:@"1"];
    NSArray *busStops = [myUser getAllBusStops];
    STAssertEquals(1, (int)[busStops count], @"Bus stops did not contain 1 bus stop");
    
}

@end
